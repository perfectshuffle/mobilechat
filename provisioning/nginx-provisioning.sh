#!/bin/sh

apt-get -y install nginx asp.net-examples mono-fastcgi-server4 mono-xsp4

/etc/init.d/mono-xsp2 stop
/etc/init.d/mono-xsp4 stop

cp /vagrant/provisioning/monoserve /etc/init.d

mkdir -p /etc/mono/fcgi/apps-available
mkdir -p /etc/mono/fcgi/apps-enabled

cp /vagrant/provisioning/www /etc/mono/fcgi/apps-enabled/www

chmod +x /etc/init.d/monoserve
update-rc.d monoserve defaults

sed -ie 's|mono/4.0|mono/4.5|g' /usr/bin/fastcgi-mono-server4
sed -ie 's|mono/4.0|mono/4.5|g' /usr/bin/xsp4

cp /vagrant/provisioning/nginx/default /etc/nginx/sites-enabled/default

/etc/init.d/monoserve start
/etc/init.d/nginx restart

# Just so we can easily see IP address etc
ifconfig

echo "All done :)"

