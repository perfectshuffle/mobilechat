﻿namespace Chat
open IntelliFactory.WebSharper
open IntelliFactory.WebSharper.Html
open IntelliFactory.WebSharper.JQuery

module ChatClient =

  [<Remote>]
  let post username msg = ChatServer.PostMessage username msg

  [<Remote>]
  let fetchNewMessages fromIndex = ChatServer.GetMessagesFrom fromIndex

  [<Remote>]
  let getGuestId() = ChatServer.GetGuestId()

  [<JavaScript>]  
  let mutable nextIndex = 0

  [<Inline("""jQuery(".messages").scrollTop(jQuery(".messages")[0].scrollHeight)""")>]  
  let scrollToBottom() = failwith "inlined"

  [<JavaScript>]
  let elements =    
    let messageInput = Input [Attr.Type "text"; Attr.Id "messageInput"]
    let messages = Div[Attr.Class "messages"]
    let messagesJ = JQuery.Of(messages.Dom)
    let username = Input [Attr.Type "text"; Attr.Id "username"]
    let submitButton =
      Button [Text "Send"] |>! OnClick(fun _ _ ->
        let msg = messageInput.Value
        if msg <> "" then
          post username.Value messageInput.Value
          messageInput.Value <- ""
          JQuery.Of(messageInput.Dom).Focus() |> ignore)
    Div [Attr.Class "chat-control"] -<
      [
        messages
        Div [Attr.Class "username"] -< [
            Label [Text "Username"; Attr.For username.Id]
            username
          ]
        Div [Attr.Class "new-message-container"] -< [            
            Div [Attr.Class "new-message"] -< [
              Label [Text "Message"; Attr.For messageInput.Id]            
              messageInput
            ]
            Div [Attr.Class "send-button"] -< [
              submitButton
            ]
          ]
      ]
    |>! OnAfterRender(fun _ ->
      async {
        let! guestId = getGuestId()
        if username.Value = "" then username.Value <- guestId
        JQuery.Of(messageInput.Dom).Focus() |> ignore
      } |> Async.StartImmediate



      let rec updateLoop() : Async<unit> =
        async {
          let! (start, finish, newData) = fetchNewMessages nextIndex

          let appendMessages n =
            let newNodes =
              newData
              |> Seq.map (fun msg ->
                Div[Attr.Class "message"; Attr.Style "display: none;"] -< [
                  Img [Attr.Src msg.GravatarUrl]
                  P [Text (msg.Email + ": " + msg.Body)]
                ])
              |> Seq.iter (fun msg -> messagesJ.Append(msg.Dom) |> ignore)
            JQuery.Of(".message").FadeIn() |> ignore            
            nextIndex <- n + 1
            scrollToBottom() |> ignore

          match (start, finish) with
          | 0, n ->
            messagesJ.Empty() |> ignore
            appendMessages n
          | s, n when s = nextIndex ->
            appendMessages n
          | _ -> ()          
                          
          do! Async.Sleep(100)
          return! updateLoop()
        }

      // Pressing enter in message box should submit form
      JQuery.Of(messageInput.Dom).Keypress(fun _ e ->
        if e.Which = 13 then JQuery.Of(submitButton.Dom).Click() |> ignore) |> ignore
      updateLoop() |> Async.Start
      )

type ChatControl() =
  inherit Web.Control()

  [<JavaScript>]  
  override this.Body =
    upcast ChatClient.elements