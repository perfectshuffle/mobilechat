﻿namespace Chat

module ChatServer =
  // Extension method for GetSlice
  type System.Collections.Generic.List<'T> with
      member x.GetSlice(a,b) = 
          let a = defaultArg a 0
          let b = defaultArg b (x.Count-1)
          [| for i in a .. b -> x.[i] |]


  type InboundMessage = {Email: string; Body : string}
  type OutboundMessage = {Email: string; Body : string; GravatarUrl : string}
  // Start index, end index, strings
  type Messages = int * int * OutboundMessage[]

  type ChatMsg =
  | Post of InboundMessage
  | GetFrom of int * AsyncReplyChannel<Messages>
  | GetNextGuestId of AsyncReplyChannel<string>

  let md5 = System.Security.Cryptography.MD5.Create()
  let hash (str:string) =
    let bytes = System.Text.Encoding.ASCII.GetBytes str
    let md5hash:byte[] = md5.ComputeHash(bytes)
    let sb = new System.Text.StringBuilder();
    for i = 0 to md5hash.Length - 1 do
      let nextByte = md5hash.[i].ToString("X2")
      sb.Append(nextByte) |> ignore<System.Text.StringBuilder>
    sb.ToString().ToLower()  

  let agent =
    MailboxProcessor<_>.Start(fun inbox ->
        let lines = ResizeArray<_>()     
        let guests = ref 0   
        let rec loop() =
          async {
            let! msg = inbox.Receive()
            match msg with
            | Post str ->
              lines.Add(str)
              return! loop()
            | GetFrom (index,replyChannel) ->
              if lines.Count = 0 || index >= lines.Count then
                replyChannel.Reply(-1,-1,[||]) //No new messages
              else
                let outboundMessages =
                  lines.[index..]
                  |> Seq.map (fun msg ->
                    let emailHash = hash (msg.Email.Trim().ToLower())
                    let gravatarUrl = "http://www.gravatar.com/avatar/" + emailHash
                    let outbound : OutboundMessage = {Email = msg.Email; Body = msg.Body; GravatarUrl = gravatarUrl}
                    outbound)
                  |> Seq.toArray
                replyChannel.Reply(index, lines.Count - 1 , outboundMessages)
              return! loop()
            | GetNextGuestId replyChannel ->
              let id = sprintf "Guest%d" !guests
              replyChannel.Reply(id)                              
              incr guests
              return! loop()
          }
        loop()
      )

  let GetMessagesFrom index =
    async {
      let! messages = agent.PostAndAsyncReply (fun replyChannel -> GetFrom(index, replyChannel))
      return messages
    }

  let PostMessage username msg =
    agent.Post (Post{Email = username; Body = msg})

  let GetGuestId () =
    async {
      let! id = agent.PostAndAsyncReply (fun replyChannel -> GetNextGuestId replyChannel)
      return id
    }